// WinCreate.cpp : Defines the entry point for the application.
//

#include "windows.h"
#include "stdio.h"
HINSTANCE g_hInstance = 0;
HANDLE g_hOutput = 0;

int g_xPos = 100;
int g_yPos = 100;
void OnPaint(HWND hWnd)
{
	char szText[] = "WM_PAINT\n";
	WriteConsole( g_hOutput, szText, strlen(szText), NULL, NULL );

	PAINTSTRUCT ps = { 0 };
	HDC hdc = BeginPaint(hWnd, &ps);
	TextOut(hdc, g_xPos, g_yPos, "hello", strlen("hello"));
	EndPaint(hWnd, &ps);
	//BeginPaint 和 EndPaint 必须用于WM_PAINT 消息的处理中
}

void OnKeyDown(HWND hWnd, WPARAM wParam)
{
	char szText[256] = { 0 };
	sprintf_s(szText, "WM_KEYDOWN:%08X\n", wParam);
	WriteConsole(g_hOutput, szText, strlen(szText),
		NULL, NULL);
	switch (wParam)
	{
	case VK_UP:
		g_yPos--;
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	case VK_DOWN:
		g_yPos++;
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	case VK_LEFT:
		g_xPos--;
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	case VK_RIGHT:
		g_xPos++;
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	}
}
void OnKeyUp(HWND hWnd, WPARAM wParam)
{
	char szText[256] = { 0 };
	sprintf_s(szText, "WM_KEYUP:%08X\n", wParam);
	WriteConsole(g_hOutput, szText, strlen(szText),
		NULL, NULL);
}
void OnChar(HWND hWnd, WPARAM wParam)
{
	char szText[256] = { 0 };
	sprintf_s(szText, "WM_CHAR:%08X\n", wParam);
	WriteConsole(g_hOutput, szText, strlen(szText),
		NULL, NULL);
}
LRESULT WINAPI WndProc(HWND hWnd, UINT nMsg,
	WPARAM wParam, LPARAM lParam)
{
	switch (nMsg)
	{
	case WM_CHAR:
		OnChar(hWnd, wParam);
		break;
	case WM_KEYDOWN:
		OnKeyDown(hWnd, wParam);
		break;
	case WM_KEYUP:
		OnKeyUp(hWnd, wParam);
		break;
	case WM_LBUTTONDOWN:
		InvalidateRect(hWnd, NULL, TRUE);
		//将窗口声明成需要重新绘制的区域（无效区域）
		//GetMessage可以检查到这个窗口的无效区域，发送
		//WM_PAINT消息。
		break;
	case WM_PAINT:
		OnPaint(hWnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hWnd, nMsg, wParam, lParam);
}

BOOL Register(LPSTR lpClassName, WNDPROC wndProc)
{
	WNDCLASSEX wce = { 0 };
	wce.cbSize = sizeof(wce);
	wce.cbClsExtra = 0;
	wce.cbWndExtra = 0;
	wce.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wce.hCursor = NULL;
	wce.hIcon = NULL;
	wce.hIconSm = NULL;
	wce.hInstance = g_hInstance;
	wce.lpfnWndProc = wndProc;
	wce.lpszClassName = lpClassName;
	wce.lpszMenuName = NULL;
	wce.style = CS_HREDRAW | CS_VREDRAW;
	ATOM nAtom = RegisterClassEx(&wce);
	if (nAtom == 0)
		return FALSE;
	return TRUE;
}

HWND CreateMain(LPSTR lpClassName, LPSTR lpWndName)
{
	HWND hWnd = CreateWindowEx(0, lpClassName, lpWndName,
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, NULL, NULL, g_hInstance,
		NULL);
	return hWnd;
}

void Display(HWND hWnd)
{
	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
}

void Message()
{
	MSG nMsg = { 0 };
	while (GetMessage(&nMsg, NULL, 0, 0))
	{
		TranslateMessage(&nMsg);
		DispatchMessage(&nMsg);
	}
}
int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	AllocConsole();
	g_hOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	g_hInstance = hInstance;
	if (!Register("Wnd", WndProc))
	{
		MessageBox(NULL, "注册失败", "Error", MB_OK);
		return 0;
	}
	HWND hWnd = CreateMain("Wnd", "Window");
	Display(hWnd);
	Message();
	return 0;
}