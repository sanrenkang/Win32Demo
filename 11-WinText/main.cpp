// WinCreate.cpp : Defines the entry point for the application.
//

#include "windows.h"
HINSTANCE g_hInstance = 0;

void OnPaint(HWND hWnd)
{
	CHAR szText[] = "hello";
	PAINTSTRUCT ps = { 0 };
	HDC hdc = BeginPaint(hWnd, &ps);
	SetTextColor(hdc, RGB(255, 0, 0));
	SetBkMode(hdc, TRANSPARENT);
	SetBkColor(hdc, RGB(0, 255, 0));

	HFONT hFont = CreateFont(30, 0, 45, 0, 900, TRUE,
		TRUE, TRUE, GB2312_CHARSET, 0, 0, 0, 0,
		"黑体");
	HGDIOBJ nOldFont = SelectObject(hdc, hFont);

	TextOut(hdc, 100, 100, szText, strlen(szText));

	RECT rcText = { 0 };
	rcText.left = 100;
	rcText.top = 150;
	rcText.right = 200;
	rcText.bottom = 200;
	//	Rectangle( hdc, 100, 150, 200, 200 );
	DrawText(hdc, szText, strlen(szText), &rcText,
		DT_CENTER | DT_VCENTER | DT_NOCLIP | DT_SINGLELINE);
	/*
	DT_WORD_ELLIPSIS - 省略号
	DT_NOPREFIX - 转义&
	DT_VCENTER、DT_BOTTOM - 指针对单行（DT_SINGLELINE）
	*/
	CHAR szExtText[] = "A中国人民";
	int nDis[] = { 10,0,10,0,10,0,10,0 };
	ExtTextOut(hdc, 100, 300, 0, NULL, szExtText,
		strlen(szExtText), nDis);

	SelectObject(hdc, nOldFont);
	DeleteObject(hFont);
	EndPaint(hWnd, &ps);
}

LRESULT WINAPI WndProc(HWND hWnd, UINT nMsg,
	WPARAM wParam, LPARAM lParam)
{
	switch (nMsg)
	{
	case WM_PAINT:
		OnPaint(hWnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hWnd, nMsg, wParam, lParam);
}

BOOL Register(LPSTR lpClassName, WNDPROC wndProc)
{
	WNDCLASSEX wce = { 0 };
	wce.cbSize = sizeof(wce);
	wce.cbClsExtra = 0;
	wce.cbWndExtra = 0;
	wce.hbrBackground = (HBRUSH)(COLOR_WINDOW + 2);
	wce.hCursor = NULL;
	wce.hIcon = NULL;
	wce.hIconSm = NULL;
	wce.hInstance = g_hInstance;
	wce.lpfnWndProc = wndProc;
	wce.lpszClassName = lpClassName;
	wce.lpszMenuName = NULL;
	wce.style = CS_HREDRAW | CS_VREDRAW;
	ATOM nAtom = RegisterClassEx(&wce);
	if (nAtom == 0)
		return FALSE;
	return TRUE;
}

HWND CreateMain(LPSTR lpClassName, LPSTR lpWndName)
{
	HWND hWnd = CreateWindowEx(0, lpClassName, lpWndName,
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, NULL, NULL, g_hInstance,
		NULL);
	return hWnd;
}

void Display(HWND hWnd)
{
	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
}

void Message()
{
	MSG nMsg = { 0 };
	while (GetMessage(&nMsg, NULL, 0, 0))
	{
		TranslateMessage(&nMsg);
		DispatchMessage(&nMsg);
	}
}
int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	g_hInstance = hInstance;
	if (!Register("Wnd", WndProc))
	{
		MessageBox(NULL, "注册失败", "Error", MB_OK);
		return 0;
	}
	HWND hWnd = CreateMain("Wnd", "Window");
	Display(hWnd);
	Message();
	return 0;
}



