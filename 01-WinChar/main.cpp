#include "stdio.h"

#define UNICODE
#include "windows.h"

//#ifdef   UNICODE     
//typedef   wchar_t   TCHAR;
//#else     
//typedef   unsigned   char   TCHAR;
//#endif     
void T_char()
{
	TCHAR *pszText = __TEXT("hello");
#ifdef UNICODE
	wprintf(L"%s\n", pszText);
#else
	printf("��-%s\n", pszText);
#endif
}


void C_char()
{
	char *pszText = "hello char";
	printf("%s\n", pszText);
}
void W_char()
{
	wchar_t *pwszText = L"hello wchar";
	int nLen = wcslen(pwszText);
	wprintf(L"%s-%d\n", pwszText, nLen);
}
void PrintUnicode()
{
	HANDLE hStd = GetStdHandle(STD_OUTPUT_HANDLE);
	for (WORD nHigh = 100; nHigh<200; nHigh++)
	{
		for (WORD nLow = 100; nLow<200; nLow++)
		{
			wchar_t nChar = nLow + 256 * nHigh;
			WriteConsole(hStd, &nChar, 1, NULL, NULL);
		}
		printf("\n");
	}
}
int main(int argc, char* argv[])
{
	//	C_char( );
	//	W_char( );
	//	T_char( );
	PrintUnicode();
	return 0;
}