#include "windows.h"
#include "stdio.h"
HANDLE g_hEvent = 0;//用于接收事件句柄
DWORD WINAPI TestProc1(LPVOID pParam)
{
	while (1)
	{
		WaitForSingleObject(g_hEvent, INFINITE);
		//		ResetEvent( g_hEvent );
		printf("*****************\n");
	}
	return 0;
}
DWORD WINAPI TestProc2(LPVOID pParam)
{
	while (1)
	{
		Sleep(1000);
		SetEvent(g_hEvent);
	}
	return 0;
}
int main(int argc, char* argv[])
{
	g_hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	HANDLE hThread[2] = { 0 };
	DWORD nID = 0;
	hThread[0] = CreateThread(NULL, 0, TestProc1,
		NULL, 0, &nID);
	hThread[1] = CreateThread(NULL, 0, TestProc2,
		NULL, 0, &nID);
	WaitForMultipleObjects(2, hThread, TRUE, INFINITE);
	CloseHandle(g_hEvent);
	CloseHandle(hThread[0]);
	CloseHandle(hThread[1]);
	return 0;
}
