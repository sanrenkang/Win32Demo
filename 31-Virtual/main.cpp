#include "windows.h"
#include "stdio.h"
void status()
{
	MEMORYSTATUS ms = { 0 };
	ms.dwLength = sizeof(ms);
	GlobalMemoryStatus(&ms);
	printf("Load:%d\n", ms.dwMemoryLoad);					// 返回一个介于0～100之间的值，用来指示当前系统内存的使用率。
	printf("PhyTotal:%u\n", ms.dwTotalPhys);				// 返回总的物理内存大小，以字节(byte)为单位。 　
	printf("PhyAvali:%u\n", ms.dwAvailPhys);				// 返回可用的物理内存大小，以字节(byte)为单位。 　　
	printf("VirTotal:%u\n", ms.dwTotalPageFile);			// 显示可以存在页面文件中的字节数。注意这个数值并不表示在页面文件在磁盘上的真实物理大小。 
	printf("VirAvali:%u\n", ms.dwAvailPageFile);			// 返回可用的页面文件大小，以字节(byte)为单位。 　　
	printf("AddTotal:%u\n", ms.dwTotalVirtual);				// 返回调用进程的用户模式部分的全部可用虚拟地址空间，以字节(byte)为单位。 　　
	printf("AddAvali:%u\n", ms.dwAvailVirtual);				// 返回调用进程的用户模式部分的实际自由可用的虚拟地址空间，以字节(byte)为单位。
	printf("*******************************\n");
}
void Virtual_Info()
{
	status();
	CHAR *pszText = (CHAR*)VirtualAlloc(
		NULL, 1024 * 1024 * 1024, MEM_COMMIT,
		PAGE_READWRITE);
	/*
	MEM_COMMIT - 连地址带内存都要
	MEM_RESERVE - 只要地址不要内存
	*/
	status();
	VirtualFree(pszText, 0, MEM_RELEASE);
	/*
	MEM_DECOMMIT - 只释放内存，不释放地址
	MEM_RELEASE - 连地址带内存 一起释放
	*/
	status();
}
void Virtual_Commit()
{
	CHAR *pszText = (CHAR*)VirtualAlloc(NULL, 4096,
		MEM_COMMIT, PAGE_READWRITE);
	strcpy_s(pszText, 4096, "hello");
	printf("%s\n", pszText);
	VirtualFree(pszText, 0, MEM_RELEASE);
}
void Virtual_Reserve()
{
	CHAR *pszText = (CHAR*)VirtualAlloc(NULL, 1024 * 1024 * 1024,
		MEM_RESERVE, PAGE_READWRITE);
	CHAR *pszText1 = (CHAR*)VirtualAlloc(pszText, 1,
		MEM_COMMIT, PAGE_READWRITE);

	strcpy_s(pszText + 4090, 1024 * 1024 * 1024, "hello");
	printf("%s\n", pszText1 + 4090);
	VirtualFree(pszText, 0, MEM_RELEASE);
}
int main(int argc, char* argv[])
{
	Virtual_Info( );
	//Virtual_Commit( );
	//Virtual_Reserve();
	Sleep(5000);
	return 0;
}

