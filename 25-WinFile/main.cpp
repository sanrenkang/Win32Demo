// WinFile.cpp : Defines the entry point for the console application.
//


#include "windows.h"
#include "stdio.h"
void GetLogic()
{
	DWORD nLogic = GetLogicalDrives();
	printf("%08X\n", nLogic);

	CHAR szLogic[256] = { 0 };
	GetLogicalDriveStrings(256, szLogic);
	printf("%s\n", szLogic);
}
void GetDir()
{
	CHAR szPath[256] = { 0 };
	GetSystemDirectory(szPath, 256);
	printf("系统目录：%s\n", szPath);

	GetCurrentDirectory(256, szPath);
	printf("当前工作目录：%s\n", szPath);

	SetCurrentDirectory("c:/");
	GetCurrentDirectory(256, szPath);
	printf("更改后的当前工作目录：%s\n", szPath);

	GetWindowsDirectory(szPath, 256);
	printf("windows目录：%s\n", szPath);
}
void Create()
{
	HANDLE hFile = CreateFile("c:/hello.txt",
		GENERIC_WRITE, FILE_SHARE_READ, NULL,
		CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	CloseHandle(hFile);
}
void Write()
{
	HANDLE hFile = CreateFile("c:/hello.txt",
		GENERIC_WRITE, FILE_SHARE_READ, NULL,
		OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL,
		NULL);
	CHAR szText[] = "hello write";
	DWORD nLen = 0;
	WriteFile(hFile, szText, strlen(szText), &nLen, NULL);
	printf("准备写入：%d, 实际写入：%d\n",
		strlen(szText), nLen);
	CloseHandle(hFile);
}
void Read()
{
	HANDLE hFile = CreateFile("c:/hello.txt",
		GENERIC_READ, FILE_SHARE_READ, NULL,
		OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	CHAR szText[256] = { 0 };
	DWORD nLen = 0;
	DWORD nHight = 0;
	DWORD nLow = GetFileSize(hFile, &nHight);

	ReadFile(hFile, szText, nLow, &nLen, NULL);
	printf("数据:%s, 准备读入:%d, 实际读入:%d\n",
		szText, nLow, nLen);
	CloseHandle(hFile);
}
void usefile()
{
	//	CopyFile( "c:/hello.txt", "c:/123/hello.txt", FALSE );
	//	DeleteFile( "c:/hello.txt" );
	MoveFile("c:/123/hello.txt", "c:/nohello.txt");
}
void Find()
{
	CHAR szPath[] = "c:/*.*";
	WIN32_FIND_DATA wfd = { 0 };
	HANDLE hFind = FindFirstFile(szPath, &wfd);
	BOOL goFind = TRUE;
	while (goFind)
	{
		if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			printf("[%s]\n", wfd.cFileName);
		}
		else {
			printf("%s\n", wfd.cFileName);
		}
		goFind = FindNextFile(hFind, &wfd);
	}
	FindClose(hFind);
}
int main(int argc, char* argv[])
{
		//GetLogic( );
		//GetDir( );
		//Create( );
		//Write( );
		//Read( );
		//usefile( );
	Find();

		Sleep(5000);
	return 0;
}






