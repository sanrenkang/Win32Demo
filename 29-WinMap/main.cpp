#include "windows.h"
#include "stdio.h"

int main(int argc, char* argv[])
{
	HANDLE hFile = CreateFile("c:/map.txt",
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ, NULL, CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL, NULL);
	HANDLE hMap = CreateFileMapping(hFile,
		NULL, PAGE_READWRITE, 0,
		1024 * 1024, "ZHANGJIWEN");
	//内存被创建出来同时内存已经创建出来，大小1M

	CHAR *pszText = (CHAR*)MapViewOfFile(
		hMap, FILE_MAP_ALL_ACCESS,
		0, 64 * 1024, 0);
	strcpy_s(pszText, 64* 1024, "hello mapping");
	//	printf( "%s\n", pszText );
	UnmapViewOfFile(pszText);
	getchar();
	CloseHandle(hMap);//一旦关闭，就没了
	CloseHandle(hFile);
	return 0;
}




