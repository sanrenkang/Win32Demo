
#include "windows.h"
#include "stdio.h"
typedef int(*Dll_add)(int m, int n);
typedef int(*Dll_sub)(int m, int n);
typedef int(*Dll_mul)(int m, int n);
int main()
{
	HINSTANCE hIns = LoadLibrary("22-CppDll.dll");
	printf("动态库实例句柄=%d\n", hIns);

	Dll_add add = (Dll_add)GetProcAddress(hIns, "cpp_add");
	printf("add=%p\n", add);
	int sum = add(3, 2);
	printf("sum=%d\n", sum);

	Dll_sub SUB = (Dll_sub)GetProcAddress(hIns, "cpp_sub");
	printf("SUB=%p\n", SUB);
	int sub = SUB(3, 2);
	printf("sub=%d\n", sub);

	//没有导出 空指针
	Dll_mul MUL = (Dll_mul)GetProcAddress(hIns, "cpp_mul");
	printf("MUL=%p\n", MUL);
	int mul = MUL(3, 2);
	printf("mul=%d\n", mul);

	FreeLibrary(hIns);
	return 0;
}