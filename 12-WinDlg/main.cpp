// WinCreate.cpp : Defines the entry point for the application.
//

#include "windows.h"
#include "resource.h"
HINSTANCE g_hInstance = 0;

int CALLBACK DlgProc(HWND hwndDlg, UINT nMsg,
	WPARAM wParam, LPARAM lParam)
{
	switch (nMsg)
	{
	case WM_INITDIALOG:
		MessageBox(NULL, "WM_INITDIALOG", "Infor", MB_OK);
		return TRUE;
		break;
	case WM_CREATE:
		MessageBox(NULL, "WM_CREATE", "Infor", MB_OK);
		return TRUE;
		break;
	case WM_SYSCOMMAND:
		if (wParam == SC_CLOSE)
		{
			//EndDialog(hwndDlg, 1001);
			DestroyWindow( hwndDlg );
			return TRUE;//
		}
		break;
	}
	return FALSE;//将消息交给真正对话框处理函数做默认处理
}

void OnModel(HWND hWnd)
{
	//模式对话框一般是在栈中生成的 当模式对话框对应的对象离开生命区时即销毁对话框
	//int nRet = DialogBox(g_hInstance,
	//	MAKEINTRESOURCE(IDD_DIALOG1), hWnd, DlgProc);
	//if (nRet == 1001)
	//{
	//	MessageBox(NULL, "SUCCESSFUL", "Infor", MB_OK);
	//}


	//非模式对话框  一般在堆中，所以要主动用DestroyWindow销毁它。
	HWND hDlg = CreateDialog(g_hInstance,
		MAKEINTRESOURCE(IDD_DIALOG1), hWnd,
		DlgProc);
	ShowWindow(hDlg, SW_SHOW);
}
void OnCommand(HWND hWnd, WPARAM wParam)
{
	switch (LOWORD(wParam))
	{
	case ID_MODEL:
		OnModel(hWnd);
		break;
	}
}
LRESULT WINAPI WndProc(HWND hWnd, UINT nMsg,
	WPARAM wParam, LPARAM lParam)
{
	switch (nMsg)
	{
	case WM_COMMAND:
		OnCommand(hWnd, wParam);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hWnd, nMsg, wParam, lParam);
}

BOOL Register(LPSTR lpClassName, WNDPROC wndProc)
{
	WNDCLASSEX wce = { 0 };
	wce.cbSize = sizeof(wce);
	wce.cbClsExtra = 0;
	wce.cbWndExtra = 0;
	wce.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wce.hCursor = NULL;
	wce.hIcon = NULL;
	wce.hIconSm = NULL;
	wce.hInstance = g_hInstance;
	wce.lpfnWndProc = wndProc;
	wce.lpszClassName = lpClassName;
	wce.lpszMenuName = MAKEINTRESOURCE(IDR_MENU1);
	wce.style = CS_HREDRAW | CS_VREDRAW;
	ATOM nAtom = RegisterClassEx(&wce);
	if (nAtom == 0)
		return FALSE;
	return TRUE;
}

HWND CreateMain(LPSTR lpClassName, LPSTR lpWndName)
{
	HWND hWnd = CreateWindowEx(0, lpClassName, lpWndName,
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, NULL, NULL, g_hInstance,
		NULL);
	return hWnd;
}

void Display(HWND hWnd)
{
	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
}

void Message()
{
	MSG nMsg = { 0 };
	while (GetMessage(&nMsg, NULL, 0, 0))
	{
		TranslateMessage(&nMsg);
		DispatchMessage(&nMsg);
	}
}
int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	g_hInstance = hInstance;
	if (!Register("Wnd", WndProc))
	{
		MessageBox(NULL, "注册失败", "Error", MB_OK);
		return 0;
	}
	HWND hWnd = CreateMain("Wnd", "Window");
	Display(hWnd);
	Message();
	return 0;
}