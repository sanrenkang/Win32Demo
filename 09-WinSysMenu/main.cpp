// WinCreate.cpp : Defines the entry point for the application.
//

#include "windows.h"
HINSTANCE g_hInstance = 0;

void OnCreate(HWND hWnd)
{
	HMENU hSysMenu = GetSystemMenu(hWnd, FALSE);
	for (int i = 0; i<6; i++)
	{
		DeleteMenu(hSysMenu, i, MF_BYPOSITION);
	}
	AppendMenu(hSysMenu, MF_STRING, 1001, "我的菜单项");
}
void OnSysCommand(HWND hWnd, WPARAM wParam)
{
	switch (LOWORD(wParam))
	{
	case 1001:
		MessageBox(NULL, "我的菜单项被点击", "Infor",
			MB_OK);
		break;
	}
}
void OnRButtonUp(HWND hWnd, LPARAM lParam)
{
	/*	HMENU hPopup = CreatePopupMenu( );
	AppendMenu( hPopup, MF_STRING, 1002, "新建" );
	AppendMenu( hPopup, MF_SEPARATOR, 0, "" );
	AppendMenu( hPopup, MF_STRING, 1003, "退出" );
	POINT pt = { 0 };
	pt.x = LOWORD(lParam);
	pt.y = HIWORD(lParam);
	ClientToScreen( hWnd, &pt );

	BOOL nRet = TrackPopupMenu( hPopup,
	TPM_CENTERALIGN|TPM_VCENTERALIGN|TPM_RETURNCMD,
	pt.x, pt.y, 0, hWnd, NULL );
	switch( nRet )
	{
	case 1002:
	MessageBox( NULL, "没走循环，点了新建", "Infor", MB_OK );
	break;
	case 1003:
	MessageBox( NULL, "没走循环，点了退出", "Infor", MB_OK );
	break;
	}*/
}
void OnCommand(HWND hWnd, WPARAM wParam)
{
	switch (LOWORD(wParam))
	{
	case 1002:
		MessageBox(NULL, "点了新建", "Infor", MB_OK);
		break;
	case 1003:
		MessageBox(NULL, "点了退出", "Infor", MB_OK);
		break;
	}
}
//一般WM_RBUTTONUP都会伴随着WM_CONTEXTMENU
//OnRButtonUp 中的参数point 是相对与窗口的左上角坐标（relative to the upper-left corner of the window.MSDN）
//OnContextMenu  中的参数point 是屏幕坐标（Position of the cursor, in screen coordinates）
void OnContextMenu(HWND hWnd, LPARAM lParam)
{
	HMENU hPopup = CreatePopupMenu();
	AppendMenu(hPopup, MF_STRING, 1002, "新建");
	AppendMenu(hPopup, MF_SEPARATOR, 0, "");
	AppendMenu(hPopup, MF_STRING, 1003, "退出");
	POINT pt = { 0 };
	pt.x = LOWORD(lParam);
	pt.y = HIWORD(lParam);
	//	ClientToScreen( hWnd, &pt );

	BOOL nRet = TrackPopupMenu(hPopup,
		TPM_CENTERALIGN | TPM_VCENTERALIGN | TPM_RETURNCMD,
		pt.x, pt.y, 0, hWnd, NULL);
	switch (nRet)
	{
	case 1002:
		MessageBox(NULL, "没走循环，点了新建", "Infor", MB_OK);
		break;
	case 1003:
		MessageBox(NULL, "没走循环，点了退出", "Infor", MB_OK);
		break;
	}
}
LRESULT WINAPI WndProc(HWND hWnd, UINT nMsg,
	WPARAM wParam, LPARAM lParam)
{
	switch (nMsg)
	{
	case WM_CONTEXTMENU:
		OnContextMenu(hWnd, lParam);
		break;
	case WM_COMMAND:
		OnCommand(hWnd, wParam);
		break;
	case WM_RBUTTONUP:
		OnRButtonUp(hWnd, lParam);
		break;
	case WM_SYSCOMMAND:
		OnSysCommand(hWnd, wParam);
		break;
	case WM_CREATE:
		OnCreate(hWnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hWnd, nMsg, wParam, lParam);
}

BOOL Register(LPSTR lpClassName, WNDPROC wndProc)
{
	WNDCLASSEX wce = { 0 };
	wce.cbSize = sizeof(wce);
	wce.cbClsExtra = 0;
	wce.cbWndExtra = 0;
	wce.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wce.hCursor = NULL;
	wce.hIcon = NULL;
	wce.hIconSm = NULL;
	wce.hInstance = g_hInstance;
	wce.lpfnWndProc = wndProc;
	wce.lpszClassName = lpClassName;
	wce.lpszMenuName = NULL;
	wce.style = CS_HREDRAW | CS_VREDRAW;
	ATOM nAtom = RegisterClassEx(&wce);
	if (nAtom == 0)
		return FALSE;
	return TRUE;
}

HWND CreateMain(LPSTR lpClassName, LPSTR lpWndName)
{
	HWND hWnd = CreateWindowEx(0, lpClassName, lpWndName,
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, NULL, NULL, g_hInstance,
		NULL);
	return hWnd;
}

void Display(HWND hWnd)
{
	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
}

void Message()
{
	MSG nMsg = { 0 };
	while (GetMessage(&nMsg, NULL, 0, 0))
	{
		TranslateMessage(&nMsg);
		DispatchMessage(&nMsg);
	}
}
int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	g_hInstance = hInstance;
	if (!Register("Wnd", WndProc))
	{
		MessageBox(NULL, "注册失败", "Error", MB_OK);
		return 0;
	}
	HWND hWnd = CreateMain("Wnd", "Window");
	Display(hWnd);
	Message();
	return 0;
}