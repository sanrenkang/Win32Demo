// WinCreate.cpp : Defines the entry point for the application.
//

#include "windows.h"
#include "stdio.h"
HINSTANCE g_hInstance = 0;

//窗口处理函数
LRESULT CALLBACK WndProc(HWND hWnd, UINT nMsg,
	WPARAM wParam, LPARAM lParam)
{
	switch (nMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hWnd, nMsg, wParam, lParam);
}
//注册窗口类
BOOL Register(LPSTR lpClassName, WNDPROC wndProc)
{
	WNDCLASSEX wce = { 0 };
	wce.cbSize = sizeof(wce);
	wce.cbClsExtra = 200;
	wce.cbWndExtra = 200;
	wce.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wce.hCursor = NULL;
	wce.hIcon = NULL;
	wce.hIconSm = NULL;
	wce.hInstance = g_hInstance;
	wce.lpfnWndProc = wndProc;
	wce.lpszClassName = lpClassName;
	wce.lpszMenuName = NULL;
	wce.style = CS_HREDRAW | CS_VREDRAW;
	ATOM nAtom = RegisterClassEx(&wce);
	if (nAtom == 0)
		return FALSE;
	return TRUE;
}
//创建主窗口
HWND CreateMain(LPSTR lpClassName, LPSTR lpWndName)
{
	HWND hWnd = CreateWindowEx(0, lpClassName, lpWndName,
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, NULL, NULL, g_hInstance,
		NULL);
	return hWnd;
}
//创建子窗口
HWND CreateChild(LPSTR lpClassName, LPSTR lpWndName,
	HWND hParent)
{
	HWND hChild = CreateWindowEx(0, lpClassName, lpWndName,
		WS_CHILD | WS_VISIBLE | WS_OVERLAPPEDWINDOW,
		100, 100, 200, 200, hParent, NULL,
		g_hInstance, NULL);
	return hChild;
}
//显示窗口
void Display(HWND hWnd)
{
	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
}
//消息循环
void Message()
{
	MSG nMsg = { 0 };
	while (GetMessage(&nMsg, NULL, 0, 0))
	{
		TranslateMessage(&nMsg);
		DispatchMessage(&nMsg);
	}
}
void SetExtra(HWND hWnd)
{
	char *pszText = "hello buffer";
	//绑定类 如static
	SetClassLong(hWnd, 196, (LONG)pszText);
	//绑定对象 
	SetWindowLong(hWnd, 0, 100L);
}
void GetExtra(HWND hWnd)
{
	LONG nClassExtra = GetClassLong(hWnd, 196);
	char *pszText = (char*)nClassExtra;

	LONG nWndExtra = GetWindowLong(hWnd, 0);
	CHAR szText[256] = { 0 };
	sprintf_s(szText, "窗口类:%s, 窗口:%d", pszText,
		nWndExtra);
	MessageBox(NULL, szText, "Infor", MB_OK);
}
int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	g_hInstance = hInstance;
	if (!Register("Wnd", WndProc))
	{
		MessageBox(NULL, "注册失败", "Error", MB_OK);
		return 0;
	}
	HWND hWnd = CreateMain("Wnd", "Window");
	Register("CHILD", DefWindowProc);
	HWND hChild1 = CreateChild("CHILD", "c1", hWnd);
	HWND hChild2 = CreateChild("CHILD", "c2", hWnd);
	SetExtra(hChild1);
	GetExtra(hChild2);

	Display(hWnd);
	MoveWindow(hChild1, 310, 100, 200, 200, TRUE);
	MoveWindow(hChild2, 510, 100, 200, 200, TRUE);
	Message();
	return 0;
}



