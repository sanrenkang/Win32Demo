#include "stdio.h"
#include "../23-ClassDll/dllclass.h"
_declspec(dllimport)int cpp_add(int add1, int add2);
_declspec(dllimport)int cpp_sub(int sub1, int sub2);
_declspec(dllimport)int cpp_mul(int mul1, int mul2);
#pragma comment( lib, "../Debug/22-CppDll.lib" )

extern "C" _declspec(dllimport)int c_add(int add1, int add2);
extern "C" _declspec(dllimport)int c_sub(int sub1, int sub2);
#pragma comment( lib, "../Debug/21-CDll.lib" )

#pragma comment( lib, "../Debug/23-ClassDll.lib" )

int main()
{
	int sum = cpp_add(5, 9);
	int sub = cpp_sub(5, 9);
	int mul = cpp_mul(5, 9);
	printf("sum=%d,sub=%d,mul=%d\n", sum, sub, mul);

	sum = c_add(5, 4);
	sub = c_sub(5, 4);
	printf("sum=%d, sub=%d\n", sum, sub);

	CMath math;
	sum = math.add(5, 3);
	sub = math.sub(5, 3);
	printf("sum=%d, sub1=%d\n", sum, sub);
	return 0;
}