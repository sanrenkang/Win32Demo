#include "stdio.h"
int cpp_add(int add1, int add2);
int cpp_sub(int sub1, int sub2);
#pragma comment( lib, "../Debug/19-CppLib.lib" )

extern "C" int c_add(int add1, int add2);
extern "C" int c_sub(int sub1, int sub2);
#pragma comment( lib, "../Debug/18-CLib.lib" )
int main()
{
	int sum = cpp_add(8, 5);
	int sub = cpp_sub(8, 5);
	printf("sum=%d, sub=%d\n", sum, sub);

	sum = c_add(5, 6);
	sub = c_sub(5, 6);
	printf("sum=%d, sub=%d\n", sum, sub);
	return 0;
}