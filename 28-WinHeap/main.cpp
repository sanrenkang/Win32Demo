#include "windows.h"
#include "stdio.h"

void Heap_Info()
{
	HANDLE nHeap = GetProcessHeap();
	printf("第一个堆：%d\n", nHeap);
	HANDLE nHeaps[256] = { 0 };
	DWORD nCount = GetProcessHeaps(256, nHeaps);
	for (int i = 0; i<nCount; i++)
	{
		printf("%d-%d\n", i, nHeaps[i]);
	}
}

void Heap()
{
	HANDLE hHeap = HeapCreate(HEAP_NO_SERIALIZE,
		1024 * 1024, 0);
	printf("自己的堆：%d\n", hHeap);


	Heap_Info();
	CHAR *pszText = (CHAR*)HeapAlloc(hHeap,
		HEAP_ZERO_MEMORY, 2 * 1024 * 1024);
	strcpy_s(pszText, 2 * 1024 *1024, "hello heap");
	printf("%s\n", pszText);
	Heap_Info();
	//用来释放堆内存
	HeapFree(hHeap, HEAP_NO_SERIALIZE, pszText);
	HeapDestroy(hHeap);
}

int main(int argc, char* argv[])
{
	Heap();
	Sleep(5000);
	return 0;
}



