// WinCreate.cpp : Defines the entry point for the application.
//

#include "windows.h"
#include "stdio.h"
HINSTANCE g_hInstance = 0;
HANDLE g_hOutput = 0;

int g_xPos = 50;
int g_yPos = 50;
#define ELL_LEN 50

BOOL Left_Right = 1;
BOOL Right_Left = 0;
BOOL Up_Down = 1;
BOOL Down_Up = 0;

VOID CALLBACK TimerProc(HWND hwnd, UINT uMsg,
	UINT idEvent, DWORD dwTime)
{
	CHAR szText[256] = { 0 };
	sprintf_s(szText, "定时器:%d\n", idEvent);
	WriteConsole(g_hOutput, szText, strlen(szText),
		NULL, NULL);
}

void OnTimer(HWND hWnd, WPARAM wParam)
{
	CHAR szText[256] = { 0 };
	sprintf_s(szText, "窗口处理函数处理 定时器：%d\n",
		wParam);
	WriteConsole(g_hOutput, szText, strlen(szText),
		NULL, NULL);
	RECT rc = { 0 };
	GetClientRect(hWnd, &rc);//获取边界信息
	if (g_xPos <= rc.left)
	{
		Left_Right = 1;
		Right_Left = 0;
	}
	else if (g_xPos >= rc.right - ELL_LEN)
	{
		Left_Right = 0;
		Right_Left = 1;
	}
	if (Left_Right == 1)
	{
		g_xPos++;
	}
	else if (Right_Left == 1)
	{
		g_xPos--;
	}

	if (g_yPos <= rc.top)
	{
		Up_Down = 1;
		Down_Up = 0;
	}
	else if (g_yPos >= rc.bottom - ELL_LEN)
	{
		Down_Up = 1;
		Up_Down = 0;
	}

	if (Up_Down == 1)
	{
		g_yPos++;
	}
	else if (Down_Up == 1)
	{
		g_yPos--;
	}
	InvalidateRect(hWnd, NULL, FALSE);
}
void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps = { 0 };
	HDC hdc = BeginPaint(hWnd, &ps);
	//画圆
	Ellipse(hdc, g_xPos, g_yPos,
		g_xPos + ELL_LEN, g_yPos + ELL_LEN);
	EndPaint(hWnd, &ps);
}
LRESULT WINAPI WndProc(HWND hWnd, UINT nMsg,
	WPARAM wParam, LPARAM lParam)
{
	switch (nMsg)
	{
	case WM_PAINT:
		OnPaint(hWnd);
		break;
	case WM_TIMER:
		OnTimer(hWnd, wParam);
		break;
	case WM_CREATE:
	{
		SetTimer(hWnd, 1, 10, NULL);
		//			SetTimer( hWnd, 2, 2000, TimerProc );
		//最后一个参数是否为NULL,将影响 这个定时器
		//触发WM_TIMER消息，找谁处理。
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hWnd, nMsg, wParam, lParam);
}

BOOL Register(LPSTR lpClassName, WNDPROC wndProc)
{
	WNDCLASSEX wce = { 0 };
	wce.cbSize = sizeof(wce);
	wce.cbClsExtra = 0;
	wce.cbWndExtra = 0;
	wce.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wce.hCursor = NULL;
	wce.hIcon = NULL;
	wce.hIconSm = NULL;
	wce.hInstance = g_hInstance;
	wce.lpfnWndProc = wndProc;
	wce.lpszClassName = lpClassName;
	wce.lpszMenuName = NULL;
	wce.style = CS_HREDRAW | CS_VREDRAW;
	ATOM nAtom = RegisterClassEx(&wce);
	if (nAtom == 0)
		return FALSE;
	return TRUE;
}

HWND CreateMain(LPSTR lpClassName, LPSTR lpWndName)
{
	HWND hWnd = CreateWindowEx(0, lpClassName, lpWndName,
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, NULL, NULL, g_hInstance,
		NULL);
	return hWnd;
}

void Display(HWND hWnd)
{
	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
}

void Message()
{
	MSG nMsg = { 0 };
	while (GetMessage(&nMsg, NULL, 0, 0))
	{
		TranslateMessage(&nMsg);
		DispatchMessage(&nMsg);
	}
}
int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	AllocConsole();
	g_hOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	g_hInstance = hInstance;
	if (!Register("Wnd", WndProc))
	{
		MessageBox(NULL, "注册失败", "Error", MB_OK);
		return 0;
	}
	HWND hWnd = CreateMain("Wnd", "Window");
	Display(hWnd);
	Message();
	return 0;
}



