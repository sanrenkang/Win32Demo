#include "windows.h"
#include "stdio.h"
DWORD g_value = 0;
DWORD WINAPI TestProc1(LPVOID pParam)
{
	for (int i = 0; i<100000000; i++)
	{
		InterlockedIncrement((long*)&g_value);
	}
	return 0;
}
DWORD WINAPI TestProc2(LPVOID pParam)
{
	for (int i = 0; i<100000000; i++)
	{
		InterlockedIncrement((long*)&g_value);
	}
	return 0;
}

int main(int argc, char* argv[])
{
	DWORD nID = 0;
	HANDLE hThread[2] = { 0 };
	hThread[0] = CreateThread(NULL, 0, TestProc1, NULL,
		0, &nID);
	hThread[1] = CreateThread(NULL, 0, TestProc2, NULL,
		0, &nID);
	WaitForMultipleObjects(2, hThread, TRUE, INFINITE);
	printf("%d\n", g_value);//��ӡ���
	CloseHandle(hThread[0]);
	CloseHandle(hThread[1]);
	return 0;
}
