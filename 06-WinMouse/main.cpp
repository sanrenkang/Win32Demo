

#include "windows.h"
#include "stdio.h"
HINSTANCE g_hInstance = 0;
HANDLE g_hOutput = 0;
int g_xPos = 100;
int g_yPos = 100;
void OnLButtonDown(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	CHAR szText[256] = { 0 };
	sprintf_s(szText, "WM_LBUTTONDOWN:����=%08X, x=%d,y=%d\n",
		wParam, LOWORD(lParam), HIWORD(lParam));
	WriteConsole(g_hOutput, szText, strlen(szText),
		NULL, NULL);
}
void OnLButtonUp(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	CHAR szText[256] = { 0 };
	sprintf_s(szText, "WM_LBUTTONUP:����=%08X, x=%d,y=%d\n",
		wParam, LOWORD(lParam), HIWORD(lParam));
	WriteConsole(g_hOutput, szText, strlen(szText),
		NULL, NULL);
}
void OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	CHAR szText[256] = { 0 };
	sprintf_s(szText, "WM_MOUSEMOVE:����=%08X,x=%d,y=%d\n",
		wParam, LOWORD(lParam), HIWORD(lParam));
	//	WriteConsole( g_hOutput, szText, strlen(szText),
	//					NULL, NULL );
	g_xPos = LOWORD(lParam);
	g_yPos = HIWORD(lParam);
	InvalidateRect(hWnd, NULL, TRUE);
}
void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps = { 0 };
	HDC hdc = BeginPaint(hWnd, &ps);
	TextOut(hdc, g_xPos, g_yPos, "hello", 5);
	EndPaint(hWnd, &ps);
}
void OnLButtonDblClk(HWND hWnd)
{
	char szText[] = "WM_LBUTTONDBLCLK\n";
	WriteConsole(g_hOutput, szText, strlen(szText),
		NULL, NULL);
}
void OnMouseWheel(HWND hWnd, WPARAM wParam)
{
	short nDelta = HIWORD(wParam);//����ƫ����
	CHAR szText[256] = { 0 };
	sprintf_s(szText, "ƫ����=%d\n", nDelta);
	WriteConsole(g_hOutput, szText, strlen(szText),
		NULL, NULL);
}
LRESULT WINAPI WndProc(HWND hWnd, UINT nMsg,
	WPARAM wParam, LPARAM lParam)
{
	switch (nMsg)
	{
	case WM_MOUSEWHEEL:
		OnMouseWheel(hWnd, wParam);
		break;
	case WM_LBUTTONDBLCLK:
		OnLButtonDblClk(hWnd);
		break;
	case WM_PAINT:
		OnPaint(hWnd);
		break;
	case WM_MOUSEMOVE:
		OnMouseMove(hWnd, wParam, lParam);
		break;
	case WM_LBUTTONDOWN:
		OnLButtonDown(hWnd, wParam, lParam);
		break;
	case WM_LBUTTONUP:
		OnLButtonUp(hWnd, wParam, lParam);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hWnd, nMsg, wParam, lParam);
}

BOOL Register(LPSTR lpClassName, WNDPROC wndProc)
{
	WNDCLASSEX wce = { 0 };
	wce.cbSize = sizeof(wce);
	wce.cbClsExtra = 0;
	wce.cbWndExtra = 0;
	wce.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wce.hCursor = NULL;
	wce.hIcon = NULL;
	wce.hIconSm = NULL;
	wce.hInstance = g_hInstance;
	wce.lpfnWndProc = wndProc;
	wce.lpszClassName = lpClassName;
	wce.lpszMenuName = NULL;
	wce.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	ATOM nAtom = RegisterClassEx(&wce);
	if (nAtom == 0)
		return FALSE;
	return TRUE;
}

HWND CreateMain(LPSTR lpClassName, LPSTR lpWndName)
{
	HWND hWnd = CreateWindowEx(0, lpClassName, lpWndName,
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, NULL, NULL, g_hInstance,
		NULL);
	return hWnd;
}

void Display(HWND hWnd)
{
	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
}

void Message()
{
	MSG nMsg = { 0 };
	while (GetMessage(&nMsg, NULL, 0, 0))
	{
		TranslateMessage(&nMsg);
		DispatchMessage(&nMsg);
	}
}
int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	AllocConsole();
	g_hOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	g_hInstance = hInstance;
	if (!Register("Wnd", WndProc))
	{
		MessageBox(NULL, "ע��ʧ��", "Error", MB_OK);
		return 0;
	}
	HWND hWnd = CreateMain("Wnd", "Window");
	Display(hWnd);
	Message();
	return 0;
}



