
#include "windows.h"
#include "stdio.h"
void Env()
{
	CHAR *pszEnv = (CHAR*)GetEnvironmentStrings();
	CHAR *pszText = pszEnv;
	while (pszText[0])
	{
		printf("%s\n", pszText);
		pszText = pszText + strlen(pszText) + 1;
	}
	FreeEnvironmentStrings(pszEnv);
}
void Variable()
{
	SetEnvironmentVariable("TAM1210", "GOOD");
	CHAR szText[256] = { 0 };
	GetEnvironmentVariable("TAM1210", szText, 256);
	printf("%s\n", szText);
}
void Proc_Info()
{
	DWORD nID = GetCurrentProcessId();
	HANDLE nHandle = GetCurrentProcess();
	CHAR szText[256] = { 0 };
	sprintf_s(szText, "进程ID=%d, 进程句柄:%d\n",
		nID, nHandle);
	printf("%s\n", szText);
	getchar();
}
void Process()
{
	STARTUPINFO si = { 0 };
	PROCESS_INFORMATION pi = { 0 };
	CreateProcess("c:/windows/system32/calc.exe", "",
		NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
	printf("进程句柄：%d, 进程ID=%d\n",
		pi.hProcess, pi.dwProcessId);
	WaitForSingleObject(pi.hProcess, INFINITE);
	printf("wait over!!!!\n");
}
void Kill()
{
	HANDLE hProcess = OpenProcess(
		PROCESS_ALL_ACCESS, FALSE, 3916);
	TerminateProcess(hProcess, 1000);
}
int main(int argc, char* argv[])
{
	//Env( );
	//Variable( );
	//Proc_Info( );
	//Process();
	Kill( );
	return 0;
}
