// WinCreate.cpp : Defines the entry point for the application.
//

#include "windows.h"
#include "resource.h"
#include "stdlib.h"
#include "stdio.h"
HINSTANCE g_hInstance = 0;

void OnCreate(HWND hWnd)
{
	HWND hEdit = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "",
		WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL | ES_MULTILINE |
		ES_AUTOVSCROLL | WS_HSCROLL | WS_VSCROLL,
		0, 0, 200, 200, hWnd, (HMENU)1001,
		g_hInstance, NULL);
	/*
	ES_PASSWORD - 只针对单行风格。
	ES_NUMBER - 只能输入 数字键
	*/
	HFONT hFont = CreateFont(50, 0, 0, 0, 1800, 0, 0, 0,
		GB2312_CHARSET, 0, 0, 0, 0, "黑体");
	SendMessage(hEdit, WM_SETFONT, (WPARAM)hFont, 1);
}
void OnSize(HWND hWnd, LPARAM lParam)
{
	int nWidth = LOWORD(lParam);
	int nHight = HIWORD(lParam);
	HWND hEdit = GetDlgItem(hWnd, 1001);
	MoveWindow(hEdit, 0, 0, nWidth, nHight, TRUE);
}

void OnSave(HWND hWnd)
{
	HWND hEdit = GetDlgItem(hWnd, 1001);
	LRESULT nLen = SendMessage(hEdit, WM_GETTEXTLENGTH,
		0, 0);
	CHAR *pszText = (CHAR*)malloc(nLen + 1);
	memset(pszText, 0, nLen + 1);
	SendMessage(hEdit, WM_GETTEXT, nLen + 1, (LPARAM)pszText);
	FILE *pFile;
	fopen_s(&pFile, "c:/my.txt", "w");
	fwrite(pszText, 1, nLen, pFile);
	fclose(pFile);
	free(pszText);
	SetWindowText(hWnd, "window");
}
void OnOpen(HWND hWnd)
{
	FILE *pFile;
	fopen_s(&pFile, "c:/my.txt", "r");
	fseek(pFile, 0, SEEK_END);
	long nLen = ftell(pFile);
	CHAR *pszText = (CHAR*)malloc(nLen + 1);
	memset(pszText, 0, nLen + 1);
	fseek(pFile, 0, SEEK_SET);
	fread(pszText, 1, nLen, pFile);
	HWND hEdit = GetDlgItem(hWnd, 1001);
	SendMessage(hEdit, WM_SETTEXT, 0, (LPARAM)pszText);
	fclose(pFile);
	free(pszText);
}
void OnCommand(HWND hWnd, WPARAM wParam)
{
	switch (LOWORD(wParam))
	{
	case 1001:
	{
		if (HIWORD(wParam) == EN_CHANGE)
		{
			SetWindowText(hWnd, "window *");
		}
	}
	break;
	case ID_SAVE:
		OnSave(hWnd);
		break;
	case ID_OPEN:
		OnOpen(hWnd);
		break;
	}
}

HBRUSH OnCtlColorEdit(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	HDC hdcEdit = (HDC)wParam;//获取在编辑框中绘图的绘图设备
	SetBkColor(hdcEdit, RGB(255, 0, 0));
	SetTextColor(hdcEdit, RGB(0, 255, 0));
	return CreateSolidBrush(RGB(255, 0, 0));
}
LRESULT WINAPI WndProc(HWND hWnd, UINT nMsg,
	WPARAM wParam, LPARAM lParam)
{
	switch (nMsg)
	{
	case WM_CTLCOLOREDIT:
		return (LRESULT)OnCtlColorEdit(hWnd, wParam, lParam);
		break;
	case WM_COMMAND:
		OnCommand(hWnd, wParam);
		break;
	case WM_SIZE:
		OnSize(hWnd, lParam);
		break;
	case WM_CREATE:
		OnCreate(hWnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hWnd, nMsg, wParam, lParam);
}

BOOL Register(LPSTR lpClassName, WNDPROC wndProc)
{
	WNDCLASSEX wce = { 0 };
	wce.cbSize = sizeof(wce);
	wce.cbClsExtra = 0;
	wce.cbWndExtra = 0;
	wce.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wce.hCursor = NULL;
	wce.hIcon = NULL;
	wce.hIconSm = NULL;
	wce.hInstance = g_hInstance;
	wce.lpfnWndProc = wndProc;
	wce.lpszClassName = lpClassName;
	wce.lpszMenuName = MAKEINTRESOURCE(IDR_MENU1);
	wce.style = CS_HREDRAW | CS_VREDRAW;
	ATOM nAtom = RegisterClassEx(&wce);
	if (nAtom == 0)
		return FALSE;
	return TRUE;
}

HWND CreateMain(LPSTR lpClassName, LPSTR lpWndName)
{
	HWND hWnd = CreateWindowEx(0, lpClassName, lpWndName,
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, NULL, NULL, g_hInstance,
		NULL);
	return hWnd;
}

void Display(HWND hWnd)
{
	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
}

void Message()
{
	MSG nMsg = { 0 };
	while (GetMessage(&nMsg, NULL, 0, 0))
	{
		TranslateMessage(&nMsg);
		DispatchMessage(&nMsg);
	}
}
int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	g_hInstance = hInstance;
	if (!Register("Wnd", WndProc))
	{
		MessageBox(NULL, "注册失败", "Error", MB_OK);
		return 0;
	}
	HWND hWnd = CreateMain("Wnd", "Window");
	Display(hWnd);
	Message();
	return 0;
}



