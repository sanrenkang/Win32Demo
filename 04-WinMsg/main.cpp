// WinCreate.cpp : Defines the entry point for the application.
//

#include "windows.h"
#include "stdio.h"
HINSTANCE g_hInstance = 0;
HANDLE g_hOutput = 0;//接受标准输出句柄
HWND hEdit = 0;

#define WM_MYMESSAGE WM_USER+1000

void OnCreate(HWND hWnd)
{
	hEdit = CreateWindowEx(0, "EDIT", "OK",
		WS_CHILD | WS_VISIBLE | WS_BORDER, 0, 0, 100, 100,
		hWnd, NULL, g_hInstance, NULL);
}


void OnSize(HWND hWnd, LPARAM lParam)
{
	int nWidth = LOWORD(lParam);
	int nHight = HIWORD(lParam);
	char szText[256] = { 0 };
	sprintf_s(szText, "WM_SIZE: 宽=%d,高=%d\n",
		nWidth, nHight);
	WriteConsole(g_hOutput, szText, strlen(szText),
		NULL, NULL);
}

void OnMyMessage(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	char szText[256] = { 0 };
	sprintf_s(szText, "wParam=%d, lParam=%d", wParam, lParam);
	MessageBox(NULL, szText, "Infor", MB_OK);
}

LRESULT WINAPI WndProc(HWND hWnd, UINT nMsg,
	WPARAM wParam, LPARAM lParam)
{
	switch (nMsg)
	{
	case WM_MYMESSAGE:
		OnMyMessage(hWnd, wParam, lParam);
		break;
	case WM_LBUTTONDOWN:
		//投递信息并处理完才会继续执行下去
		SendMessage(hWnd, WM_MYMESSAGE, 100, 200);
		break;
	case WM_CREATE:
		OnCreate(hWnd);
		break;
	case WM_SIZE:
		OnSize(hWnd, lParam);
		break;
	case WM_DESTROY:
		//		PostQuitMessage( 0 );
		//只投递信息 
		PostMessage(hWnd, WM_QUIT, 0, 0);
		break;
	}
	return DefWindowProc(hWnd, nMsg, wParam, lParam);
}

BOOL Register(LPSTR lpClassName, WNDPROC wndProc)
{
	WNDCLASSEX wce = { 0 };
	wce.cbSize = sizeof(wce);
	wce.cbClsExtra = 0;
	wce.cbWndExtra = 0;
	wce.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wce.hCursor = NULL;
	wce.hIcon = NULL;
	wce.hIconSm = NULL;
	wce.hInstance = g_hInstance;
	wce.lpfnWndProc = wndProc;
	wce.lpszClassName = lpClassName;
	wce.lpszMenuName = NULL;
	wce.style = CS_HREDRAW | CS_VREDRAW;
	ATOM nAtom = RegisterClassEx(&wce);
	if (nAtom == 0)
		return FALSE;
	return TRUE;
}

HWND CreateMain(LPSTR lpClassName, LPSTR lpWndName)
{
	char *pszText = "hello";
	HWND hWnd = CreateWindowEx(WS_EX_CLIENTEDGE, lpClassName, lpWndName,
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, NULL, NULL, g_hInstance,
		pszText);
	return hWnd;
}

void Display(HWND hWnd)
{
	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
}

void Message()
{
	MSG nMsg = { 0 };
	while (GetMessage(&nMsg, NULL, 0, 0))
	{
		TranslateMessage(&nMsg);
		DispatchMessage(&nMsg);
	}
}
int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	AllocConsole();
	g_hOutput = GetStdHandle(STD_OUTPUT_HANDLE);

	g_hInstance = hInstance;
	if (!Register("Wnd", WndProc))
	{
		MessageBox(NULL, "注册失败", "Error", MB_OK);
		return 0;
	}
	HWND hWnd = CreateMain("Wnd", "Window");
	Display(hWnd);
	Message();
	return 0;
}



