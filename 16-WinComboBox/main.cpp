// WinCreate.cpp : Defines the entry point for the application.
//

#include "windows.h"
#include "resource.h"
#include "stdio.h"
HINSTANCE g_hInstance = 0;

void OnCreate(HWND hWnd)
{
	HWND hSimple = CreateWindowEx(0, "COMBOBOX", "Simple",
		WS_CHILD | WS_VISIBLE | CBS_SIMPLE | WS_VSCROLL,
		50, 50, 200, 200,
		hWnd, (HMENU)1001, g_hInstance, NULL);
	CreateWindowEx(0, "COMBOBOX", "DropDown",
		WS_CHILD | WS_VISIBLE | CBS_DROPDOWN | WS_VSCROLL,
		260, 50, 200, 200,
		hWnd, (HMENU)1002, g_hInstance, NULL);
	CreateWindowEx(0, "COMBOBOX", "DropList",
		WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST | WS_VSCROLL,
		470, 50, 200, 200,
		hWnd, (HMENU)1003, g_hInstance, NULL);

	HFONT hFont = CreateFont(30, 0, 0, 0, 1800, 0, 0, 0,
		GB2312_CHARSET, 0, 0, 0, 0, "黑体");
	SendMessage(hSimple, WM_SETFONT, (WPARAM)hFont, 1);
}
void OnAdd(HWND hWnd)
{
	HWND hSimple = GetDlgItem(hWnd, 1001);
	HWND hDropDown = GetDlgItem(hWnd, 1002);
	HWND hDropList = GetDlgItem(hWnd, 1003);
	for (int i = 0; i<100; i++)
	{
		CHAR szText[256] = { 0 };
		sprintf_s(szText, "Item%d", i);
		SendMessage(hSimple, CB_ADDSTRING, 0, (LPARAM)szText);
		SendMessage(hDropDown, CB_ADDSTRING, 0, (LPARAM)szText);
		SendMessage(hDropList, CB_ADDSTRING, 0, (LPARAM)szText);

		SendMessage(hSimple, CB_SETITEMDATA, i, 1000 + i);
	}
	SendMessage(hSimple, CB_SETCURSEL, 99, 0);
	SendMessage(hDropDown, CB_SETCURSEL, 99, 0);
	SendMessage(hDropList, CB_SETCURSEL, 99, 0);
}
void OnClear(HWND hWnd)
{
	HWND hSimple = GetDlgItem(hWnd, 1001);
	HWND hDropDown = GetDlgItem(hWnd, 1002);
	HWND hDropList = GetDlgItem(hWnd, 1003);
	SendMessage(hSimple, CB_RESETCONTENT, 0, 0);
	SendMessage(hDropDown, CB_RESETCONTENT, 0, 0);
	SendMessage(hDropList, CB_RESETCONTENT, 0, 0);

}

void OnDelete(HWND hWnd)
{
	HWND hSimple = GetDlgItem(hWnd, 1001);
	HWND hDropDown = GetDlgItem(hWnd, 1002);
	HWND hDropList = GetDlgItem(hWnd, 1003);
	LRESULT nSel = SendMessage(hSimple, CB_GETCURSEL, 0, 0);
	if (nSel != CB_ERR)
	{
		SendMessage(hSimple, CB_DELETESTRING, nSel, 0);
	}
	nSel = SendMessage(hDropDown, CB_GETCURSEL, 0, 0);
	if (nSel != CB_ERR)
	{
		SendMessage(hDropDown, CB_DELETESTRING, nSel, 0);
	}
	nSel = SendMessage(hDropList, CB_GETCURSEL, 0, 0);
	if (nSel != CB_ERR)
	{
		SendMessage(hDropList, CB_DELETESTRING, nSel, 0);
	}
}
void OnFind(HWND hWnd)
{
	HWND hSimple = GetDlgItem(hWnd, 1001);
	CHAR szText[] = "Item";
	LRESULT nFind = SendMessage(hSimple,
		CB_SELECTSTRING, -1, (LPARAM)szText);
	/*
	CB_FINDSTRING - 非精确匹配查找，可以不全，但不能错
	CB_FINDSTRINGEXACT - 精确匹配查找，必须写全
	CB_SELECTSTRING - 非精确匹配查找，将匹配到的选项设置为
	当前选择项
	*/
	if (nFind != CB_ERR)
	{
		CHAR szFind[256] = { 0 };
		sprintf_s(szFind, "匹配到的选项=%d", nFind);
		MessageBox(NULL, szFind, "Infor", MB_OK);
	}
	else {
		MessageBox(NULL, "没有匹配到", "Infor", MB_OK);
	}
}
void OnText(HWND hWnd)
{
	HWND hSimple = GetDlgItem(hWnd, 1001);
	CHAR szText[256] = { 0 };
	LRESULT nSel = SendMessage(hSimple, CB_GETCURSEL, 0, 0);
	if (nSel != CB_ERR)
	{
		SendMessage(hSimple, CB_GETLBTEXT,
			nSel, (LPARAM)szText);
		MessageBox(NULL, szText, "Infor", MB_OK);
	}
	else {
		MessageBox(NULL, "没有选择项", "Infor", MB_OK);
	}

	CHAR szText1[256] = { 0 };
	SendMessage(hSimple, WM_GETTEXT, 256, (LPARAM)szText1);
	MessageBox(NULL, szText1, "Infor", MB_OK);
}
void OnData(HWND hWnd)
{
	HWND hSimple = GetDlgItem(hWnd, 1001);
	LRESULT nSel = SendMessage(hSimple, CB_GETCURSEL, 0, 0);
	if (nSel != CB_ERR)
	{
		LRESULT nData = SendMessage(hSimple, CB_GETITEMDATA,
			nSel, 0);
		CHAR szText[256] = { 0 };
		sprintf_s(szText, "附加数据=%d", nData);
		MessageBox(NULL, szText, "Infor", MB_OK);
	}
}
void OnCommand(HWND hWnd, WPARAM wParam)
{
	switch (LOWORD(wParam))
	{
	case 1001:
	{
		if (HIWORD(wParam) == CBN_SELCHANGE)
		{
			HWND hSimple = GetDlgItem(hWnd, 1001);
			LRESULT nSel = SendMessage(hSimple,
				CB_GETCURSEL, 0, 0);
			HWND hDropDown = GetDlgItem(hWnd, 1002);
			HWND hDropList = GetDlgItem(hWnd, 1003);
			SendMessage(hDropDown, CB_SETCURSEL,
				nSel, 0);
			SendMessage(hDropList, CB_SETCURSEL,
				nSel, 0);
		}
	}
	break;
	case ID_DATA:
		OnData(hWnd);//获取选择项附加数据
		break;
	case ID_TEXT:
		OnText(hWnd);//获取选择项文本内容
		break;
	case ID_FIND:
		OnFind(hWnd);//匹配选项
		break;
	case ID_DELETE:
		OnDelete(hWnd);//删除选择项
		break;
	case ID_CLEAR:
		OnClear(hWnd);//清空选项
		break;
	case ID_ADD:
		OnAdd(hWnd);//添加选项
		break;
	}
}
LRESULT WINAPI WndProc(HWND hWnd, UINT nMsg,
	WPARAM wParam, LPARAM lParam)
{
	switch (nMsg)
	{
	case WM_COMMAND:
		OnCommand(hWnd, wParam);
		break;
	case WM_CREATE:
		OnCreate(hWnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hWnd, nMsg, wParam, lParam);
}

BOOL Register(LPSTR lpClassName, WNDPROC wndProc)
{
	WNDCLASSEX wce = { 0 };
	wce.cbSize = sizeof(wce);
	wce.cbClsExtra = 0;
	wce.cbWndExtra = 0;
	wce.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wce.hCursor = NULL;
	wce.hIcon = NULL;
	wce.hIconSm = NULL;
	wce.hInstance = g_hInstance;
	wce.lpfnWndProc = wndProc;
	wce.lpszClassName = lpClassName;
	wce.lpszMenuName = MAKEINTRESOURCE(IDR_MENU1);
	wce.style = CS_HREDRAW | CS_VREDRAW;
	ATOM nAtom = RegisterClassEx(&wce);
	if (nAtom == 0)
		return FALSE;
	return TRUE;
}

HWND CreateMain(LPSTR lpClassName, LPSTR lpWndName)
{
	HWND hWnd = CreateWindowEx(0, lpClassName, lpWndName,
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, NULL, NULL, g_hInstance,
		NULL);
	return hWnd;
}

void Display(HWND hWnd)
{
	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
}

void Message()
{
	MSG nMsg = { 0 };
	while (GetMessage(&nMsg, NULL, 0, 0))
	{
		TranslateMessage(&nMsg);
		DispatchMessage(&nMsg);
	}
}
int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	g_hInstance = hInstance;
	if (!Register("Wnd", WndProc))
	{
		MessageBox(NULL, "注册失败", "Error", MB_OK);
		return 0;
	}
	HWND hWnd = CreateMain("Wnd", "Window");
	Display(hWnd);
	Message();
	return 0;
}



