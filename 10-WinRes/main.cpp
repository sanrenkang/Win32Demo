// WinCreate.cpp : Defines the entry point for the application.
//

#include "windows.h"
#include "resource.h"
HINSTANCE g_hInstance = 0;

void OnCommand(HWND hWnd, WPARAM  wParam)
{
	switch (LOWORD(wParam))
	{
	case ID_NEW:
		MessageBox(NULL, "点了新建", "Infor", MB_OK);
		break;
	case ID_QUIT:
		MessageBox(NULL, "点了退出", "Infor", MB_OK);
		break;
	case ID_ABOUT:
		MessageBox(NULL, "点了关于", "Infor", MB_OK);
		break;
	}
}
void OnPaint(HWND hWnd)
{
	HICON hIcon = LoadIcon(g_hInstance,
		MAKEINTRESOURCE(IDI_ICON1));
	PAINTSTRUCT ps = { 0 };
	HDC hdc = BeginPaint(hWnd, &ps);
	DrawIcon(hdc, 100, 100, hIcon);
	EndPaint(hWnd, &ps);
}
BOOL OnSetCursor(HWND hWnd, LPARAM lParam)
{
	//	HCURSOR hRect = LoadCursor( g_hInstance,
	//						MAKEINTRESOURCE(IDC_CURSOR1) );
	//	HCURSOR hBall = LoadCursor( g_hInstance, 
	//						MAKEINTRESOURCE(IDC_CURSOR2) );

	HCURSOR hRect = LoadCursorFromFile(
		"c:/windows/Cursors/dinosaur.ani");
	HCURSOR hBall = LoadCursorFromFile(
		"c:/windows/Cursors/dinosau2.ani");
	if (LOWORD(lParam) == HTCLIENT)
	{
		POINT pt = { 0 };
		GetCursorPos(&pt);
		ScreenToClient(hWnd, &pt);
		RECT rc = { 0 };
		GetClientRect(hWnd, &rc);
		if (pt.x <= rc.right / 2)
		{
			SetCursor(hBall);
		}
		else {
			SetCursor(hRect);
		}
		return TRUE;
	}
	return FALSE;
}
LRESULT WINAPI WndProc(HWND hWnd, UINT nMsg,
	WPARAM wParam, LPARAM lParam)
{
	switch (nMsg)
	{
	case WM_SETCURSOR:
		if (OnSetCursor(hWnd, lParam))
		{
			return 0;
		}
		break;
	case WM_PAINT:
		OnPaint(hWnd);
		break;
	case WM_COMMAND:
		OnCommand(hWnd, wParam);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hWnd, nMsg, wParam, lParam);
}

BOOL Register(LPSTR lpClassName, WNDPROC wndProc)
{
	WNDCLASSEX wce = { 0 };
	wce.cbSize = sizeof(wce);
	wce.cbClsExtra = 0;
	wce.cbWndExtra = 0;
	wce.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wce.hCursor = LoadCursor(g_hInstance,
		MAKEINTRESOURCE(IDC_CURSOR1));
	wce.hIcon = LoadIcon(g_hInstance,
		MAKEINTRESOURCE(IDI_ICON1));
	wce.hIconSm = LoadIcon(g_hInstance,
		MAKEINTRESOURCE(IDI_ICON2));
	wce.hInstance = g_hInstance;
	wce.lpfnWndProc = wndProc;
	wce.lpszClassName = lpClassName;
	wce.lpszMenuName = NULL;//MAKEINTRESOURCE(IDR_MENU1);
	wce.style = CS_HREDRAW | CS_VREDRAW;
	ATOM nAtom = RegisterClassEx(&wce);
	if (nAtom == 0)
		return FALSE;
	return TRUE;
}

HWND CreateMain(LPSTR lpClassName, LPSTR lpWndName)
{
	HMENU hMenu = LoadMenu(g_hInstance,
		MAKEINTRESOURCE(IDR_MENU1));
	HWND hWnd = CreateWindowEx(0, lpClassName, lpWndName,
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, NULL, hMenu, g_hInstance,
		NULL);
	HICON hIcon = LoadIcon(g_hInstance,
		MAKEINTRESOURCE(IDI_ICON1));
	SendMessage(hWnd, WM_SETICON,
		ICON_SMALL, (LPARAM)hIcon);
	return hWnd;
}

void Display(HWND hWnd)
{
	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
}

void Message()
{
	MSG nMsg = { 0 };
	while (GetMessage(&nMsg, NULL, 0, 0))
	{
		TranslateMessage(&nMsg);
		DispatchMessage(&nMsg);
	}
}
int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	g_hInstance = hInstance;
	if (!Register("Wnd", WndProc))
	{
		MessageBox(NULL, "注册失败", "Error", MB_OK);
		return 0;
	}
	char szTitle[256] = { 0 };
	LoadString(g_hInstance, IDS_WND, szTitle, 256);
	HWND hWnd = CreateMain("Wnd", szTitle);
	Display(hWnd);
	Message();
	return 0;
}