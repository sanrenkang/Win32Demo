
#define DLLCLASS_EXPORTS
#include "dllclass.h"
#include "windows.h"
#include "stdio.h"
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason,
	LPVOID lpvReserved)
{
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		printf("Loading... ...\n");
		break;
	case DLL_PROCESS_DETACH:
		printf("UnLoading... ...\n");
		break;
	}
	return TRUE;
}

int CMath::add(int add1, int add2)
{
	return add1 + add2;
}

int CMath::sub(int sub1, int sub2)
{
	return sub1 - sub2;
}

