#pragma once

#ifdef DLLCLASS_EXPORTS
#define EXT_CLASS _declspec(dllexport)
#else
#define EXT_CLASS _declspec(dllimport)
#endif

class EXT_CLASS CMath
{
public:
	int add(int add1, int add2);
	int sub(int sub1, int sub2);
};

