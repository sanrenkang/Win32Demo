// WinCreate.cpp : Defines the entry point for the application.
//

#include "windows.h"
HINSTANCE g_hInstance = 0;

int g_state = 0;//标志量
HMENU hFile = 0;
void OnCreate(HWND hWnd)
{
	HMENU hMain = CreateMenu();

	hFile = CreatePopupMenu();
	AppendMenu(hFile, MF_STRING | MF_CHECKED, 1003, "新建");
	AppendMenu(hFile, MF_SEPARATOR, 0, "");
	AppendMenu(hFile, MF_STRING | MF_MENUBREAK, 1004, "退出");

	HMENU hHelp = CreatePopupMenu();
	AppendMenu(hHelp, MF_STRING | MF_GRAYED, 1005, "关于");

	AppendMenu(hMain, MF_POPUP, (UINT)hFile, "文件");
	AppendMenu(hMain, MF_POPUP, (UINT)hHelp, "帮助");
	SetMenu(hWnd, hMain);
}
void OnCommand(HWND hWnd, WPARAM wParam)
{
	switch (LOWORD(wParam))
	{
	case 1003:
	{
		if (g_state == 0)
		{
			CheckMenuItem(hFile, 1003,
				MF_BYCOMMAND | MF_UNCHECKED);
		}
		else {
			CheckMenuItem(hFile, 0,
				MF_BYPOSITION | MF_CHECKED);
		}
		g_state = !g_state;
	}
	break;
	case 1004:
		MessageBox(NULL, "点了退出", "Infor", MB_OK);
		break;
	case 1005:
		MessageBox(NULL, "点了关于", "Infor", MB_OK);
		break;
	}
}
LRESULT WINAPI WndProc(HWND hWnd, UINT nMsg,
	WPARAM wParam, LPARAM lParam)
{
	switch (nMsg)
	{
	case WM_COMMAND:
		OnCommand(hWnd, wParam);
		break;
	case WM_CREATE:
		OnCreate(hWnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hWnd, nMsg, wParam, lParam);
}

BOOL Register(LPSTR lpClassName, WNDPROC wndProc)
{
	WNDCLASSEX wce = { 0 };
	wce.cbSize = sizeof(wce);
	wce.cbClsExtra = 0;
	wce.cbWndExtra = 0;
	wce.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wce.hCursor = NULL;
	wce.hIcon = NULL;
	wce.hIconSm = NULL;
	wce.hInstance = g_hInstance;
	wce.lpfnWndProc = wndProc;
	wce.lpszClassName = lpClassName;
	wce.lpszMenuName = NULL;
	wce.style = CS_HREDRAW | CS_VREDRAW;
	ATOM nAtom = RegisterClassEx(&wce);
	if (nAtom == 0)
		return FALSE;
	return TRUE;
}

HWND CreateMain(LPSTR lpClassName, LPSTR lpWndName)
{
	HWND hWnd = CreateWindowEx(0, lpClassName, lpWndName,
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, NULL, NULL, g_hInstance,
		NULL);
	return hWnd;
}

void Display(HWND hWnd)
{
	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
}

void Message()
{
	MSG nMsg = { 0 };
	while (GetMessage(&nMsg, NULL, 0, 0))
	{
		TranslateMessage(&nMsg);
		DispatchMessage(&nMsg);
	}
}
int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	g_hInstance = hInstance;
	if (!Register("Wnd", WndProc))
	{
		MessageBox(NULL, "注册失败", "Error", MB_OK);
		return 0;
	}
	HWND hWnd = CreateMain("Wnd", "Window");
	Display(hWnd);
	Message();
	return 0;
}



